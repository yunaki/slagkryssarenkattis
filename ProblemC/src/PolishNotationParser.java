import java.util.Deque;

class PolishNotationParser {
    private String NON_DECIMAL = "\\D";
    private String temp1, temp2;
    private Deque<String> stack;

    PolishNotationParser(Deque<String> stack) {
        this.stack = stack;
    }

    void parse(String[] expression) {
        for (int i = lengthOf(expression); i >= 0; i--) {
            if (calculable()) {
                if (expression[i].matches(NON_DECIMAL)) {
                    switch (expression[i]) {
                        case "+":
                            if (decimals(stack)) {
                                push(addition(temp1, temp2));
                                break;
                            }
                            push(expression[i]);
                            break;
                        case "-":
                            if (decimals(stack)) {
                                push(subtraction(temp1, temp2));
                                break;
                            }
                            push(expression[i]);
                            break;
                        case "*":
                            if (decimals(stack)) {
                                push(multiplication(temp1, temp2));
                                break;
                            }
                            push(expression[i]);
                            break;
                        default:
                            push(expression[i]);
                    }
                } else {
                    push(expression[i]);
                }
            } else {
                push(expression[i]);
            }
        }
    }

    private boolean decimals (Deque<String> stack) {
        temp1 = stack.pop();
        temp2 = stack.pop();
        if (temp1.matches(NON_DECIMAL) || temp2.matches(NON_DECIMAL)) {
            stack.push(temp2);
            stack.push(temp1);
            return false;
        }
        return true;
    }

    private int lengthOf(String[] e) {
        return e.length-1;
    }

    private void push(String s) {
        stack.push(s);
    }

    private boolean calculable() {
        return stack.size() >= 2;
    }

    private String addition(String x, String y) {
        return String.valueOf(Integer.parseInt(x) + Integer.parseInt(y));
    }
    private String subtraction(String x, String y) {
        return String.valueOf(Integer.parseInt(x) - Integer.parseInt(y));
    }
    private String multiplication(String x, String y) {
        return String.valueOf(Integer.parseInt(x) * Integer.parseInt(y));
    }
}
