import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        int count = 0;
        String curLine;
        Deque<String> stack = new ArrayDeque<>();
        PolishNotationParser p = new PolishNotationParser(stack);
        BufferedReader br;

        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            while ((curLine = br.readLine()) != null) {
                count++;
                String[] expression = curLine.split(" ");
                p.parse(expression);

                System.out.print("Case " + count + ": ");
                while (!stack.isEmpty()) {
                    System.out.print(stack.pop() + " ");
                }
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
