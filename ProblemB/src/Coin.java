class Coin {
    private int cVal;
    private int itVal;


    Coin(String cVal, String itVal) {
        this.cVal = Integer.parseInt(cVal);
        this.itVal = Integer.parseInt(itVal);
    }

    int getCVal() {
        return cVal;
    }
    int getItVal() {
        return itVal;
    }
}
