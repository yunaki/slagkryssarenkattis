import java.io.*;

public class Main {
    public static void main(String[] args) {
        int problems;
        String curLine;
        String[] elements;
        Coin[] coinTypes;
        BufferedReader br;
        EModulus em = new EModulus();

        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            problems = Integer.parseInt(br.readLine());
            String s;
            for (int i = 0; i < problems; i++) {
                curLine = br.readLine();
                elements = curLine.split(" ");
                coinTypes = new Coin[Integer.parseInt(elements[0])];
                s = elements[1];
                for (int j = 0; j < coinTypes.length; j++) {
                    curLine = br.readLine();
                    elements = curLine.split(" ");
                    coinTypes[j] = new Coin(elements[0], elements[1]);
                }
                System.out.println(em.solve(Integer.parseInt(s), coinTypes));
                br.readLine();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
