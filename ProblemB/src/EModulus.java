class EModulus {
    String solve(int s, Coin[] coinTypes) {
        int MAX = 301;
        int coins = MAX;
        int goal = s*s;
        int xVal = s+1;
        int yVal = s+1;

        int graph[][] = new int[xVal][yVal];

        for (int i = 0; i < xVal; i++) {
            for (int j = 0; j < yVal; j++) {
                graph[i][j] = 301;
            }
        }
        graph[0][0] = 0;

        int eMod, newLength;
        for (int x = 0; x < xVal; x++) {
            for (int y = 0; y < yVal; y++) {
                eMod = x*x+y*y;
                for (Coin c : coinTypes) {
                    if (x-c.getCVal() >= 0 && y-c.getItVal() >= 0) {
                        newLength = graph[x-c.getCVal()][y-c.getItVal()] + 1;
                        if (newLength < graph[x][y] && graph[x-c.getCVal()][y-c.getItVal()] != MAX) {
                            graph[x][y] = newLength;
                            if (eMod == goal && graph[x][y] < coins) {
                                coins = graph[x][y];
                                yVal = y;
                            } else if ( eMod > goal) {
                                yVal = y;
                            }
                        }
                    }
                }
            }
        }

        if (coins == 301) {
            return "not possible";
        }
        return Integer.toString(coins);
    }
}
