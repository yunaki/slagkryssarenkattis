import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        int n, k, line;
        String[] requestsInfo;
        Deque<Integer> requests;
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            requestsInfo = br.readLine().split(" ");
            n = Integer.parseInt(requestsInfo[0]);
            requests = new ArrayDeque<>();
            k = Integer.parseInt(requestsInfo[1]);
            line = Integer.parseInt(br.readLine());
            requests.add(line);
            for (int i = 1; i < n; i++) {
                line = Integer.parseInt(br.readLine());
                for (int j = 0; j < requests.size(); j++) {
                    if (requests.peekFirst()+1000 <= line) {
                        requests.removeFirst();
                        break;
                    }
                }
                requests.addLast(line);
            }
            int answer = (requests.size() + k - 1) / k;
            System.out.println(answer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
